<?php
#Name:Data To Ritchey Base Number B v1
#Description:Convert a data to a number using the Ritchey Base Number B encoding scheme. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is a path to the file to convert. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('data_to_ritchey_base_number_b_v1') === FALSE){
function data_to_ritchey_base_number_b_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@empty($string) === TRUE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert string to Base64. Break base64 string into an array of single characters. Represent each letter as an incrementing number between 11 and 82, excluding any numbers containing zero (eg: A = 11, B = 12, C = 13). Convert array to string. NOTE: Base64 can encode be done in chunks of 3 bytes and still achieve the same result as processing a file all at once, but this implementation does not process files 3 bytes at a time. There is no point since it is returning the number, not saving it to another file.]
	if (@empty($errors) === TRUE){
		###Convert to base64
		$string = @base64_encode($string);
		###Convert string to array of single characters
		$string = @str_split($string, 1);
		###Convert each character in the array to a number between 10 and 74.
		foreach ($string as &$character){
			if ($character === "A"){
				$character = '11';	
			} else if ($character === "B"){
				$character = '12';
			} else if ($character === "C"){
				$character = '13';
			} else if ($character === "D"){
				$character = '14';
			} else if ($character === "E"){
				$character = '15';
			} else if ($character === "F"){
				$character = '16';
			} else if ($character === "G"){
				$character = '17';
			} else if ($character === "H"){
				$character = '18';
			} else if ($character === "I"){
				$character = '19';	
			} else if ($character === "J"){
				$character = '21';	
			} else if ($character === "K"){
				$character = '22';	
			} else if ($character === "L"){
				$character = '23';	
			} else if ($character === "M"){
				$character = '24';	
			} else if ($character === "N"){
				$character = '25';	
			} else if ($character === "O"){
				$character = '26';	
			} else if ($character === "P"){
				$character = '27';	
			} else if ($character === "Q"){
				$character = '28';	
			} else if ($character === "R"){
				$character = '29';	
			} else if ($character === "S"){
				$character = '31';	
			} else if ($character === "T"){
				$character = '32';	
			} else if ($character === "U"){
				$character = '33';	
			} else if ($character === "V"){
				$character = '34';	
			} else if ($character === "W"){
				$character = '35';	
			} else if ($character === "X"){
				$character = '36';	
			} else if ($character === "Y"){
				$character = '37';	
			} else if ($character === "Z"){
				$character = '38';	
			} else if ($character === "a"){
				$character = '39';	
			} else if ($character === "b"){
				$character = '41';
			} else if ($character === "c"){
				$character = '42';
			} else if ($character === "d"){
				$character = '43';
			} else if ($character === "e"){
				$character = '44';
			} else if ($character === "f"){
				$character = '45';
			} else if ($character === "g"){
				$character = '46';
			} else if ($character === "h"){
				$character = '47';
			} else if ($character === "i"){
				$character = '48';	
			} else if ($character === "j"){
				$character = '49';	
			} else if ($character === "k"){
				$character = '51';	
			} else if ($character === "l"){
				$character = '52';	
			} else if ($character === "m"){
				$character = '53';	
			} else if ($character === "n"){
				$character = '54';	
			} else if ($character === "o"){
				$character = '55';	
			} else if ($character === "p"){
				$character = '56';	
			} else if ($character === "q"){
				$character = '57';	
			} else if ($character === "r"){
				$character = '58';	
			} else if ($character === "s"){
				$character = '59';	
			} else if ($character === "t"){
				$character = '61';	
			} else if ($character === "u"){
				$character = '62';	
			} else if ($character === "v"){
				$character = '63';	
			} else if ($character === "w"){
				$character = '64';	
			} else if ($character === "x"){
				$character = '65';	
			} else if ($character === "y"){
				$character = '66';	
			} else if ($character === "z"){
				$character = '67';	
			} else if ($character === "0"){
				$character = '68';	
			} else if ($character === "1"){
				$character = '69';	
			} else if ($character === "2"){
				$character = '71';	
			} else if ($character === "3"){
				$character = '72';	
			} else if ($character === "4"){
				$character = '73';	
			} else if ($character === "5"){
				$character = '74';	
			} else if ($character === "6"){
				$character = '75';	
			} else if ($character === "7"){
				$character = '76';	
			} else if ($character === "8"){
				$character = '77';	
			} else if ($character === "9"){
				$character = '78';	
			} else if ($character === "+"){
				$character = '79';	
			} else if ($character === "/"){
				$character = '81';	
			} else if ($character === "="){
				$character = '82';	
			} else {
				$errors[] = "task - Convert characters to numbers";
				goto result;
			}
		}
		###Convert array back to string.
		$string = @implode($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('data_to_ritchey_base_number_b_v1_format_error') === FALSE){
			function data_to_ritchey_base_number_b_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("data_to_ritchey_base_number_b_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>